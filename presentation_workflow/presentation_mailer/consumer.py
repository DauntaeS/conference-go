import json
import pika
from pika.exceptions import AMQPConnectionError
import django
import os
import sys
import time
from django.core.mail import send_mail

# with connection to rabbitmq error

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()


def process_message(ch, method, properties, body):
    print("  Received %r" % body)
    while True:
        try:
            parameters = pika.ConnectionParameters(host="rabbitmq")
            connection = pika.BlockingConnection(parameters)
            channel = connection.channel()
            channel.queue_declare(queue="presentation_approvals")
            channel.basic_consume(
                queue="presentation_approvals",
                on_message_callback=process_message,
                auto_ack=True,
            )
            channel.start_consuming()
        except AMQPConnectionError:
            print("Could not connect to RabbitMQ")
            time.sleep(2.0)
